---
title: Heckle
description: The Clojure(Script) validation library you deserve.
elaboration: Functionally styled for composability and reusability.
logo: /images/projects/heckle.png
repo: https://github.com/nwallace/heckle
priority: 3
categories:
  - Clojure
  - Testing
---

### About

Heckle is the Clojure(Script) validation library you deserve. There are
several popular options for validation libraries in Clojure, but none of
them worked the way I wanted them to. Validation should be simple: data
comes in, and a possibly empty collection of error messages comes out.
Individual validations should be able to look at the entire set of input
data, rather than being constrained to a single item. Since validations
are just functions that take input data and possibly return an error
message, you can use good old-fashioned functional composition to build
your validations.
