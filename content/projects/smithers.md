---
title: Smithers
description: Your trusty assistant. A generic, data-driven web event processor.
elaboration: Written in Ruby with the <a href="https://dry-rb.org/" target="_blank">dry-rb</a> toolchain.
logo: static/images/projects/smithers.svg
repo: https://gitlab.com/nwallace/smithers
priority: 2
categories:
  - Ruby
---

### About

Smithers is a data-driven web event reactor. The user configures "events"
to listen for and "reactions" to execute in response. For example, you can
configure your CI system to call a webhook and react by posting a chat
message to Matrix. Or you can subscribe to broadcast notifications on
Mixer and react by posting a chat message to a Discord channel.

### The codebase

The code is written in Ruby, using [Roda](http://roda.jeremyevans.net/)
and a lot of the [dry-rb](https://dry-rb.org/) libraries.

### Architecture

I wrote Smithers as an experiment in data-driven design. I wanted it to be
trivial to add new types of events and reactions, and it should be simple
to connect any event to any reaction. To set up a new event or reaction
type, you need only define a single Ruby class adhering to a certain
interface. Then, connecting events and reactions is just a matter of
inserting data into the database.
