---
title: ActiveRecord Block Matchers
description: A set of custom RSpec matchers for testing applications with ActiveRecord.
logo: static/images/projects/active-record-block-matchers.svg
repo: https://github.com/nwallace/active_record_block_matchers
priority: 4
categories:
  - Ruby
  - Testing
  - ActiveRecord
---

### About

This is a [Ruby gem](https://rubygems.org/gems/active_record_block_matchers)
that adds a few custom matchers to RSpec for testing some behaviors of
ActiveRecord. Specifically, it adds block matchers for `create_a` and
and `create`, which are for asseting that one or more records were created
during the block.
