---
title: get.realtor
description: The domain regsitrar for .realtor® and .realestate top-level domains.
elaboration: My day job since 2014.
logo: static/images/projects/get.realtor.svg
priority: 1
categories:
  - Ruby
  - JavaScript
---

### About

[get.realtor](https://www.get.realtor) is the site I work on for my day
job. It is a domain registrar targeting the real estate business. We sell
domains in the .realtor&reg; and .realestate top-level domains.

### Architecture

A domain registrar is fundamentally a widely distributed system. The
storefront is its own application. Names are registered with the registry.
DNS is hosted on dedicated servers. Website and email hosting is usually
provided by third parties on their own servers. There is quite a lot to
orchestrate, which presents technical callenges on top of the usability
challenges inherent in such a technical product.

### My role

I have been involved in this project since almost its inception (long
before our initial launch). We are a small team, so I have been developer,
designer, architect, systems administrator, project manager, and product
owner on this project as needed for over five years now.
