---
title: The Value of Customizable Tools
description: Efficiency-focused like Vim, Sway, and the shell, are worth the effort to learn not for speed but for customizability.
date: 2019-07-24
tags:
  - linux
  - productivity
  - tooling
  - philosophy
---

As a young programmer just starting my career, I drew heavily from Gary
Bernhardt's video series [Destroy All Software][1]. I owe a tremendous amount
to these videos, but the most immediate impression you get from watching those
videos is his masterful use of Vim and the Unix shell. To my untrained eyes,
Gary might as well have been a wizard: flying through files, slamming down code
almost as fast as he could speak... It was magical.

I've been a happy Vim and Unix user myself for probably six years now, thanks to
Gary. Over the past year or so, I've extended the lessons I took from Gary's
workflow beyond just managing my code base. I've switched to a fully
programmable desktop environment (built around [Sway][2]) and web browser
([Qutebrowser][3]) as well. I really love working this way, but it's definitely
not the right choice for everyone. After reading this, you will understand why
it's such a great fit for my type of work and whether it would be a good idea
for you too.


## Invest now, reap later

Programmers all know Vim has an infamously steep learning curve. Still, its fans
will defend it (sometimes to a fault). The time it takes to learn pays off in
the long run. Of course, [not all time-saving tools are worth the
investment][4], but for a tool you use hours and hours per day, the time savings
are significant.

To me, however, the biggest benefit I feel from using Vim is that there is very
little impedance between my thoughts and the text in my editor. For example, if
I want to globally substitute "this" for "that" in a file, I simply type
<kbd>:%s/that/this/g</kbd>. If I want to change the content of a quoted string,
I type <kbd>ci"New quote</kbd>. Notice how closely the commands map to my
intent:

| Intention | Key press |
| --- | --- |
| Globally substitute | <kbd>:%s</kbd> |
| Change a quoted string | <kbd>ci"</kbd> |

With practice, this translation from intention to key press becomes second
nature. Like learning to speak a foreign language, it takes time, but
eventually you can perform the translations without thinking about it.

You can't do that with a menu-based UI. Navigating menus is slow. It requires
moving your hand to your mouse. It forces you to read. It disrupts your thoughts
in a way Vim does not. This is the real value of using efficient tools: **they
get out of your way**.


## What processes are worth the investment?

As a web developer, I usually spend at least eight hours a day at my computer
with my text editor and browser open. I work remotely, so email, text chat,
and video conferencing are crucial to me as well. Finally, I have music playing
pretty much any time I'm not in a meeting. So for me, these aspects of my
workflow are the most valuable to optimize (roughly in order):

1. Text editing
1. Source code management
1. Web browsing & debugging
1. Email
1. Text & video chat
1. Music management

If you work differently than I do your list may be different than mine, but the
principles will apply just the same: optimize first what you do the most.

## Custom-tailored tools

There are, of course, lots of ways you might optimize your workflow, but
choosing the right tool will probably get you the best bang for your buck. The
key thing you want to consider in making your choice is **programability**.
Programs that can, themselves, be programmed (or at least deeply configured)
allow you to tailor their workflow to your precise desires.

For example, in Vim I have shortcuts for opening the corresponding unit test
file for a production code file and vice versa. This shortcut has to know how
my files are laid out on disk in order to work. They won't work in a project
that uses directory structures I haven't accounted for. But they work for my
code bases, and that's all I need.

> Like a well-fitted suit, it works amazingly for me, even though it may not
> work for you.

This is only possible because Vim is so customizable and programmable. There
are other text editors that are similarly customizable (Emacs comes to mind).
The point isn't that you should use Vim; the point is that you should use a
tool that gives you power and control.

This goes equally for other tools in your stack. Many window managers will give
you similar power and control over your desktop environment. I use Sway because
I have two monitors with different DPI settings and Sway handles that quite
well, but there are lots of other great options. Any highly programmable
environment will do.

Here are some of my recommendations if you want to dive in, but don't know where
to begin:


#### Text Editors

1. [Vim](https://www.vim.org/) or [NeoVim](https://neovim.io/)
2. [Emacs](https://www.gnu.org/software/emacs/) or [Spacemacs](http://spacemacs.org/)

#### Window Managers

1. [Sway][2] or [i3](https://i3wm.org/) &mdash; a good introduction to tiling window managers
2. [AwesomeWM](https://awesomewm.org/) &mdash; another good starter tiling WM
3. [OpenBox](http://openbox.org/) &mdash; if you prefer stacking windows to tiling
4. [bspwm](https://github.com/baskerville/bspwm) &mdash; if you appreciate simplicity
5. [xmonad](http://spacemacs.org/) &mdash; if you know or want an excuse to learn Haskell
6. [dwm](https://dwm.suckless.org/) &mdash; if you're a masochist

#### Web Browsers

1. [Qutebrowser](https://qutebrowser.org/) &mdash; a popular option, written with QT
2. [Luakit](https://luakit.github.io/) &mdash; if you prefer GTK to QT

#### Email

1. [Neomutt](https://neomutt.org/) &mdash; the popular, well established choice
2. [aerc](https://aerc-mail.org/) &mdash; more modern, but very new and sometimes unstable

#### Text Chat

1. [IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat) &mdash; most easily programmable
2. [Matrix](https://matrix.org/) &mdash; more modern features, programmable with bots and webhooks

Note: I do not recommend proprietary chat solutions like Slack because API
access is often limited on free tiers.

#### Music Management

1. [Mopidy](https://www.mopidy.com/) and [NCMPCPP](https://rybczak.net/ncmpcpp/)


## What are my custom-tailored workflows?

I'm going to leave this question for future posts. Each tool I use is worth
digging into on its own. Until then, I hope this post has sparked your own
imagination! Let me know @nwallace@librem.one how you custom-tailor your
workflow.


[1]: https://www.destroyallsoftware.com/screencasts/catalog
[2]: https://swaywm.org/
[3]: https://qutebrowser.org/
[4]: https://xkcd.com/1205/


