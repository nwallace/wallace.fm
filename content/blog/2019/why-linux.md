---
draft: true
title: Why I Use Linux
description: This is why I use Linux.
date: 2019-05-23
tags:
  - linux
  - foss
  - productivity
  - tooling
img:
  url: static/images/blog/linux.svg
  attribution:
    title: GNU/Linux logo
    by_name: Engine9
    original_url: https://commons.wikimedia.org/wiki/File:Gnu-linux_minimalistic_logo.svg
    license_name: CC BY-SA 4.0
    license_url: https://creativecommons.org/licenses/by-sa/4.0
    modification: Cropped from original
---

I love Linux.
