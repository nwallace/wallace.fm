---
title: Purism Is Doing It Right
description: With their upstream-first approach, Purism's Librem 5 should become the best alternative mobile operating system.
date: 2019-07-23
tags:
  - linux
  - hardware
  - foss
img:
  url: /images/blog/2019/librem-5-call.png
  attribution:
    title: Librem 5 Render
    by_name: Purism
    original_url: https://puri.sm/wp-content/uploads/2018/12/l5-backfront.png
    license_name: CC BY-SA 4.0
    license_url: https://creativecommons.org/licenses/by-sa/4.0
---

We're officially in the third quarter of 2019, and you know what that means:
Purism could start shipping [the Librem 5](https://puri.sm/products/librem-5/)
any day now. I pre-ordered this phone almost two years ago on its crowd funder,
and the anticipation has been mounting ever since.

A lot of the hype around this phone is due to Purism's consistent marketing,
which has been derided by some as unsubstantiated. However, I think there's a
lot to be truly excited about.

## Breaking the Duopoly

Since the emergence of the smartphone, the market has been dominated by the
Apple's iOS and Google's Android. Apple have grown their platform slowly but
precisely, leaning on high price points and vendor lock-in to make their
margins. The Android platform has traded privacy and quality to meet lower
price points.

The situation obviously works well enough for a lot of people: according to
[bankmycell.com](https://www.bankmycell.com/blog/how-many-phones-are-in-the-world),
over 70% of Americans have a smartphone, and I can tell you from personal
experience that mobile app developers are in high demand, indicating a thriving
ecosystem.

However, there are some obvious problems with this duopoly. The biggest one, in
my opinion, is that there are no open-source choices. (Yes, I know Android is
open source, but Google have been steadily replacing its open-source components
with proprietary ones for years, and without custom ROMs, it's impossible to
avoid Google's proprietary spyware.)

Why is an open-source mobile OS important? Privacy. We compromise on our privacy
basically every time we log onto the internet, but now that the internet is in
your pocket/purse, we're compromising our privacy literally 24/7. This is a
bridge too far, even for the average user. The problem is, we don't have a
viable alternative.

There have been many contenders to challenge Google and Apple, and some have
even been open-source, but none have managed to claim a significant enough
portion of the market to make money. It's expensive to develop an entire
operating system and application ecosystem. They simply can't compete.


## Purism's Differentiator

Purism is taking a different tack than others who have come before them in
two crucial respects:

1. They manufacture their own hardware
2. They leverage the existing Linux desktop ecosystem as much as possible

Making their own hardware is important because that will minimize time their
developers spend reverse-engineering drivers and maximize time they spend
developing the user-facing applications and interface. Apple takes this same
strategy, and I'm sure it's a big reason why their products so famously _just
work_&trade;.

The second point, in my opinion, is the most crucial factor in Purism's
viability and sustainability. Leveraging the existing Linux desktop ecosystem
will solve the biggest hurdle to new entrants into the mobile market: growing
an ecosystem of third-party apps.

Take Ubuntu Touch as an example. In order to try to deliver their mobile OS,
Canonical built their own display server/compositor (Mir), desktop environment
(Unity 8), and a host of brand new mobile-centric applications to run on it.
That's much too large a burden for Canonical to shoulder!

The landscape for Purism today looks quite different than it did for Canonical
when Ubuntu Touch was being born. Wayland implementations are solid. GNOME and
KDE both have good touch support they developed for tablets.

But beyond that, Purism has a fundamentally different philosophy for how to
build their platform than Canonical did:

> Purism will write their code upstream first, wherever possible.

Rather than building [a brand new application](https://open-store.io/app/dekko2.dekkoproject)
to have email on your phone, just make [an existing email client](https://source.puri.sm/Librem5/tinymail)
responsive for mobile form factors! Don't write a new clock app just for the
phone: use GNOME Clocks! It's already there! The Linux Desktop ecosystem has
apps for almost everything, and usually the UI would be the only reason you
couldn't use the app on a phone.

It's a lot easier to make an application run at small sizes than it is to
develop a new application from scratch to do the same things. That's what Purism
does. Then they push their changes back upstream so everyone can benefit.

That's why I'm still excited, two years later, about the Librem 5. Purism is
doing right. And even if the phone doesn't succeed, the Linux community will
be better off for their efforts anyway.
