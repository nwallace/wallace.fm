---
draft: true
---

SQL Escapades: regexp_split_to_table

  with x as (
      select mem_id as id, regexp_split_to_table(designations || ' ' || certifications, '\w+') from raw_nar_member_full
  ),
  y as (
    select id, count(*) - 1 as num_certs from x group by 1
  )
  select num_certs, count(*) from y group by 1 

  union

  select null, count(*) from y

  order by 1;

  num_certs |  count
 -----------+---------
          0 |  906862
          1 |  131050
          2 |   40273
          3 |   15374
          4 |    5958
          5 |    2180
          6 |     848
          7 |     316
          8 |     127
          9 |      72
         10 |      29
         11 |      11
         12 |       4
         13 |       1
         14 |       3
            | 1103108
 (15 rows)
