---
title: Aw snap! Ubuntu Core
date: 2015-01-28
tags:
  - linux
  - devops
  - containers
  - docker
---

Docker! DevOps! Containerization! Buzz words! Containerization is sweeping the community like thick-rimmed glasses and JavaScript transpilers. Since I don't want the cool kids to realize I'm a hipster-imposter, I figured I'd better get ahead of this trend while it's still cool.

But where to start?

### Ubuntu Core

I probably should start learning the basics of Docker first, but Ubuntu just released a new spin they're calling Core. Ubuntu Core, like [CoreOS](https://coreos.com/) and [Project Atomic](http://www.projectatomic.io/), is a Linux distribution optimized for running app containers. I chose to try out Ubuntu Core because I just read [Lennart Pottering's blog post about OS design](http://0pointer.net/blog/revisiting-how-we-put-together-linux-systems.html) and heard from <del>some rando on Hacker News</del> a reputable source that Ubuntu Core is an early take on that philosophy. It's got all kinds of goodness, including transactional updates, systemd and container runtime agnosticism (I have a lot of issues with Docker, and much prefer the promise of something like [Rocket](https://coreos.com/blog/rocket/)).

### Getting Down and Dirty

To try it out, download the OVA image from [this page](http://www.ubuntu.com/cloud/tools/snappy#snappy-ova) and open it with [VirtualBox](https://www.virtualbox.org/) (login and password both `ubuntu`).  There's a [quick overview of Ubuntu Core](http://developer.ubuntu.com/en/snappy/#tour) on Ubuntu's developer site that walks you through an introduction to `snappy`, the tool for managing your containers.

The guide explains how to install app containers with Docker. There's an example app for displaying [xkcd](http://xkcd.com/) comics. You can install it with:

`$ sudo snappy install xkcd-webserver`

The part the guide left out was what the heck that command does! As described [here](http://developer.ubuntu.com/en/snappy/#snap-developers), Ubuntu Core installs app files in `/apps/<package>/<version>`, then symlinks `/apps/<package>/current` to the new version (just like Capistrano).

```
$ ls -l /apps/xkcd-webserver/current
  => lrwxrwxrwx 1 clickpkg clickpkg 5 Jan 28 22:01 /apps/xkcd-webserver/current -> 0.3.1
$ find /apps/xkcd-webserver -type d
  => /apps/xkcd-webserver
     /apps/xkcd-webserver/0.3.1
     /apps/xkcd-webserver/0.3.1/.click
     /apps/xkcd-webserver/0.3.1/.click/triggers
     /apps/xkcd-webserver/0.3.1/.click/updates
     /apps/xkcd-webserver/0.3.1/.click/info
     /apps/xkcd-webserver/0.3.1/www
     /apps/xkcd-webserver/0.3.1/meta
     /apps/xkcd-webserver/0.3.1/bin
```

Ubuntu Core installs app binaries to `/apps/<package>/<version>/bin`. If we look inside the `bin` directory for our xkcd app, we'll find the python script that boots up the webserver:

```
$ less /apps/xkcd-webserver/current/bin/xkcd-webserver
  => !#/usr/bin/python3
     ...
```

In fact, the OS has already started the app for us:

```
$ ps aux | grep xkcd
  => root ... /usr/bin/python3 /apps/xkcd-webserver/0.3.1/bin/xkcd-webserver
```

To see the app running, set up a port forwarding rule with VirtualBox so you can navigate to port 80 on the virtual machine. From the VirtualBox VM Manager, click your Ubuntu Core VM on the left and choose "Settings". Under "Network" > "Port Forwarding", click the plus sign to add a new rule to forward a Host port (I chose 8080) to port 80 on the Guest. Then navigate to localhost:8080 in your browser to see the app in all its glory.

Congratulations! You're running Docker apps on Ubuntu Core! Just be careful not to tell your boss at work, unless you're ready to become "the DevOps gal/guy" ;)
