---
title: Getting Started With Ruby
date: 2014-04-03
tags:
  - ruby
  - career
  - learning
---

I'm self-taught in Ruby. I owe a huge debt to the incredible Ruby community for being able to do that. Most of the developers I know do Java or C++ (since that's what they taught in college), and when I told them I was looking for a Ruby job a lot of them were curious about the appeal. I have been trying to convert them, and I wanted to jot down my path to <del>salvation</del> learn Ruby so others could do the same.

### Get a handle on the language

There are tons of awesome resources on the web for learning Ruby, but my favorite for anyone just starting out is [Codecademy](http://www.codecademy.com/tracks/ruby). Codecademy has tracks for a lot more than just Ruby, and I've had a great experience with all of the ones I've tried. They're perfect for people who have never programmed Ruby before, and I think they would be great for people who have never programmed at all before. The tracks get more and more advanced as you go along, so there are resources here for people who may have played around with Ruby in the past as well, but don't know the language very well.

### Reach enlightenment

After getting a feel for the syntax and basic usage of the language, I moved onto the [Ruby Koans](http://rubykoans.com). The Koans are a special kind of experience. They walk you through different features of the language like your own personal Mr. Miyagi. They start to engrain the idea of test-driven development while going in-depth on language constructs and the standard library.

### Hop on the Rails

Let's be real -- people learn Ruby so they can write Rails apps. Yes, Ruby is an awesome language in its own right, but that's not why most people pick it up.  The absolute best tutorial for a beginner to learn Rails is the one by Michael Hartl. You can read his book online for free at [ruby.railstutorial.org](http://ruby.railstutorial.org). The source is on [Github](https://github.com/railstutorial) for easy reference if you make a mistake, and there are lots of [Stack Overflow](http://stackoverflow.com/questions/tagged/railstutorial.org) answers to help you along as well. Hartl's tutorial walks you through building a Twitter clone, teaching you Rails, TDD, Twitter Bootstrap, and Heroku along the way. It was invaluable for me as a way to really sink your teeth into a real Rails app.

### Bring it home

After that, you *have* to start your own project. Tutorials are great, but they hold your hand the whole way -- they don't force you answer any of your own questions or come up with your own ideas. If you don't already have a project idea in mind, do what inspired me: find a problem where someone is using an Excel spreadsheet and turn it into a web app. My aunt is a chef, and she was using Excel to track inventory and expenses, so I turned it into a Rails app.  The most important thing about the project is that it needs to be something to keep you motivated. Motivation is the largest hurdle in learning a new language or framework, so you need to be personally invested in solving your problem.

It took me about 8 months of my free time to get to a place where I really felt comfortable in Ruby and with Rails, so don't get discouraged if it takes awhile.  At the end of the day, Ruby is about **joy**. It's optimized for you, the developer. So have fun with it, and try to improve just a little everyday.
