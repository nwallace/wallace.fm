# wallace.fm

### Deployment

Hopefully I can script a better way to do this someday, but until then, we have to do things manually:

1. Compile blog for release: `hugo`
2. Change directory to the release dir: `cd public/`
3. Open the SFTP connection: `sftp user@remote-server`
4. Change directory on the remote to the release dir: `cd www`
5. Copy all the files over, skipping ones that haven't changed: `put -ru * .`
6. Exit: `bye`

For a totally clean deployment, you'd have to `rm -rf /var/www/my_webapp/www/*` before doing the SFTP stuff. Otherwise, old files will linger and remain accessible. I suppose this might be desirable, though, if those files are cached somewhere...
